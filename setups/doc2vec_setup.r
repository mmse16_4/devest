# ########## PLEASE DO NOT MODIFY# ########## #
# Instalacja pakietów i załadowanie lokalnych plików *.R
rm(list = ls())
source("infra/instalAndLoadFiles.R")
loadAndInstalPackages()
sourceRFiles()
# ########################################### #

data(inaugTexts)

set.seed(1)
train_vectors <- textVectors(
  inaugTexts,
  normalize=TRUE, #Clean the text a little
  split_token=' ', #Split on spaces
  verbose=TRUE,
  freqCutoff=.01, #Remove tokens in <1% of documents.  0.01 * 57 = .57
  absCutoff=5, #Remove tokens in <5 documents
  spellcheck=FALSE, #Don't spellcheck (not yet supported)
  remove_stopwords=TRUE, #Remove stopwords after tokenizing
  stem=TRUE, #Stem after stopword removal
  ngrams=4, #Calculate 1, 2, 3 grams
  skips=1, #Calculate skip-1-grams
  tfidf=TRUE, #Do tfidf transformation after tokenization and n-grams/skip-grams
  idf=NULL, #Compute idf based on input data
  stops=NULL, #Use default stopwroids
  pca=TRUE, #Do PCA after n-grams and skip-grams
  pca_comp=20, #Use 15 PCA components
  pca_rotation=NULL, #Calculate pca rotation based on training data
  tsne=TRUE, #Do tsen
  tsne_dims=2, #Use 2 dimensions for tsne
  tsne_perplexity=5 #Use perplexity of 5 for tsne
)
head(train_vectors$tsne_proj)


df <- data.frame(
  train_vectors$tsne_proj,
  Year = inaugCorpus$documents$Year,
  President = inaugCorpus$documents$President
)
df$Label <- paste0(df$President, ' (', substr(df$Year, 3, 4), ')')
df$Year <- as.numeric(as.character(df$Year))
p1 <- ggplot(df, aes(x=TSNE1, y=TSNE2, fill=Year, label=Label)) +
  scale_fill_gradient2(low='#d73027', mid='#ffffbf', high='#4575b4', midpoint=1900) +
  geom_point(pch=21, size=5, alpha=.80) +
  geom_point(pch=21, size=5, colour = "black") +
  geom_text(size=3, vjust=1.5, alpha=.80) +
  theme_bw()
print(p1)
