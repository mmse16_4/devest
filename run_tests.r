# ########## PLEASE DO NOT MODIFY# ########## #
# Instalacja pakietow i zaladowanie lokalnych plikow *.R
rm(list = ls())
source("infra/instalAndLoadFiles.R")
setupEnvironment()
# ##################################################### #
testDirPath <- read.properties("properties.txt")$baseTestDirectory
test_results <- test_dir(testDirPath, reporter = "summary")
print(test_results)
