# ########## PLEASE DO NOT MODIFY# ########## #
# Instalacja pakietow i zaladowanie lokalnych plikow *.R
rm(list = ls())
source("infra/instalAndLoadFiles.R")
setupEnvironment()
# ##################################################### #
# If You want to modify this file to test Your changes,
# please use run_manual,r script.


sourceFilesPaths <- paste("..",retrieveAllRFilesPaths(), sep = "/")
sourceFilesPaths<-sourceFilesPaths[- (which(sapply(sourceFilesPaths,function(elem){elem=="../R/starters.R"})))]
testFilesPaths <- retrieveAllTestFilesPaths()

baseTestDirectory <- getCustomProperty("baseTestDirectory")
setwd(baseTestDirectory)
cov <- file_coverage(sourceFilesPaths, testFilesPaths)
#cov<-package_coverage("APP")
print(cov)

#if (isCurrentPerformByJenkins) {
  codecov(coverage = cov, token = "9f95eb5a-190b-4851-9e3a-8387e5e223ef")
  message("Wyslano powiadomienie do serwisu codecov.io")
#}
setwd("..")

if (percent_coverage(cov) < as.numeric(getCustomProperty("minCoverageLevel"))) {
  message("Warning ... Coverage is to low")
}
