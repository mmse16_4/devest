package com.devest.deeplearning.lstm.support;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.apache.commons.lang.IncompleteArgumentException;
import org.junit.Test;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

public class CalculateResultMatrixTest {
	private INDArray predicted;
	private INDArray lables;

	@Test
	public void shouldSubtractTwoINDArrays() {
		predicted = Nd4j.create(4, 1, 10);
		lables = Nd4j.create(4, 1, 10);
		predicted.put(new INDArrayIndex[] { NDArrayIndex.point(0), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 3, 0, 0 }));
		predicted.put(new INDArrayIndex[] { NDArrayIndex.point(1), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 4.5, 0, 0, 0 }));
		predicted.put(new INDArrayIndex[] { NDArrayIndex.point(2), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }));
		predicted.put(new INDArrayIndex[] { NDArrayIndex.point(3), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 6, 0 }));
		lables.put(new INDArrayIndex[] { NDArrayIndex.point(0), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 2, 0, 0 }));
		lables.put(new INDArrayIndex[] { NDArrayIndex.point(1), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 5, 0, 0, 0 }));
		lables.put(new INDArrayIndex[] { NDArrayIndex.point(2), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 5 }));
		lables.put(new INDArrayIndex[] { NDArrayIndex.point(3), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 6, 0 }));

		double resultArray[] = { 1, -0.5, -4, 0 };

		// act
		double[] result = LSTMNeuralNetworkSupport.calculateResultRowForRegression(predicted, lables);

		// assert
		assertArrayEquals(resultArray, result, 0);
	}

	@Test
	public void shouldThrowException() {
		predicted = Nd4j.create(4, 1, 10);
		lables = Nd4j.create(4, 1, 10);
		predicted.put(new INDArrayIndex[] { NDArrayIndex.point(0), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 3, 2, 0 }));
		predicted.put(new INDArrayIndex[] { NDArrayIndex.point(1), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 4.5, 0, 0, 0 }));
		predicted.put(new INDArrayIndex[] { NDArrayIndex.point(2), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 }));
		predicted.put(new INDArrayIndex[] { NDArrayIndex.point(3), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 6, 0 }));
		lables.put(new INDArrayIndex[] { NDArrayIndex.point(0), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 2, 0, 0 }));
		lables.put(new INDArrayIndex[] { NDArrayIndex.point(1), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 5, 0, 0, 0 }));
		lables.put(new INDArrayIndex[] { NDArrayIndex.point(2), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 5 }));
		lables.put(new INDArrayIndex[] { NDArrayIndex.point(3), NDArrayIndex.point(0), NDArrayIndex.all() },
				Nd4j.create(new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 6, 0 }));

		// act
		try {
			LSTMNeuralNetworkSupport.calculateResultRowForRegression(predicted, lables);
			fail("Exception expected!");
		} catch (IncompleteArgumentException e) {
			assertTrue(e.getMessage().contains("rozne"));
		}
	}
}
