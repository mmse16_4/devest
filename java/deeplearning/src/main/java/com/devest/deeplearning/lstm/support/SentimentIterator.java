package com.devest.deeplearning.lstm.support;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

import com.google.common.base.Preconditions;

public class SentimentIterator implements DataSetIterator {
	private final NeuralNetworkMode neuralNetworkMode;
	private final WordVectors wordVectors;
	private final int batchSize;
	private final int vectorSize;
	private final int truncateLength;
	private final int numberOfOutputsNeurons;

	private int cursor = 0;
	private final File[] inputFiles;
	private final File[] outputFiles;
	private final TokenizerFactory tokenizerFactory;
	private Set<String> unknownWords=new HashSet<>();

	/**
	 * @param dataDirectory
	 *            the directory of the IMDB review data set
	 * @param wordVectors
	 *            WordVectors object
	 * @param batchSize
	 *            Size of each minibatch for training
	 * @param truncateLength
	 *            If reviews exceed
	 * @param train
	 *            If true: return the training data. If false: return the
	 *            testing data.
	 */
	public SentimentIterator(NeuralNetConfigurationDs ds, WordVectors wordVectors, int truncateLength, boolean train)
			throws IOException {
		this.batchSize = ds.getBatchSize();
		this.neuralNetworkMode = ds.getNeuralNetworkMode();
		this.numberOfOutputsNeurons = ds.getNumberOfOutputsNeurons();
		this.vectorSize = wordVectors.lookupTable().layerSize();

		File input = new File(FilenameUtils.concat(ds.getDataPath(), (train ? "train" : "test") + "/input/") + "/");
		File output = new File(FilenameUtils.concat(ds.getDataPath(), (train ? "train" : "test") + "/output/") + "/");

		inputFiles = input.listFiles();
		outputFiles = output.listFiles();
		Preconditions.checkArgument(inputFiles.length == outputFiles.length);
		this.wordVectors = wordVectors;
		this.truncateLength = truncateLength;

		tokenizerFactory = new DefaultTokenizerFactory();
		tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
	}

	@Override
	public DataSet next(int num) {
		if (cursor >= inputFiles.length)
			throw new NoSuchElementException();
		try {
			return nextDataSet(num);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private DataSet nextDataSet(int num) throws IOException {
		// First: load reviews to String. Alternate positive and negative
		// reviews
		List<String> storys = new ArrayList<>(num);
		List<Integer> target = new ArrayList<>(num); // target, dla regresji
														// jest to licza godzin,
														// dla klasyfikacji
														// liczba 1
		List<Integer> pasitionOfTarget = new ArrayList<>(num); // neuron
																// wyj�ciowy.
																// Dla regresji
																// musi by�
																// warto�� 0,
																// dla
																// klasyfikacji
																// numer klasy
		for (int i = 0; i < num && cursor < totalExamples(); i++) {
			// Load story
			String story = FileUtils.readFileToString(inputFiles[cursor]);
			storys.add(story);
			// Load spent time
			String targetLine = FileUtils.readLines(outputFiles[cursor]).get(0);
			Integer targetValue = null;
			Integer indexOfNeuron = null;
			if (neuralNetworkMode == NeuralNetworkMode.REGRESSION) {
				try {
					targetValue = Integer.parseInt(targetLine);
					indexOfNeuron = 0;
				} catch (NumberFormatException e) {
					targetValue = Integer.valueOf(Double.valueOf(Double.parseDouble(targetLine)).intValue());
				}
			} else {
				targetValue = 1; // nauron ma miec wyj�cie 1
				indexOfNeuron = targetLine.indexOf('1');
			}
			pasitionOfTarget.add(indexOfNeuron);
			target.add(targetValue);
			cursor++;
		}

		// Second: tokenize reviews and filter out unknown words
		List<List<String>> allTokens = new ArrayList<>(storys.size());
		int maxLength = 0;
		for (String s : storys) {
			List<String> tokens = tokenizerFactory.create(s).getTokens();
			List<String> tokensFiltered = new ArrayList<>();
			for (String t : tokens) {
				if (wordVectors.hasWord(t)){
					tokensFiltered.add(t);
				}else{
					unknownWords.add(t);
				}
			}
			allTokens.add(tokensFiltered);
			maxLength = Math.max(maxLength, tokensFiltered.size());
		}

		// If longest review exceeds 'truncateLength': only take the first
		// 'truncateLength' words
		if (maxLength > truncateLength)
			maxLength = truncateLength;

		// Create data for training
		// Here: we have reviews.size() examples of varying lengths
		INDArray features = Nd4j.create(storys.size(), vectorSize, maxLength);
		INDArray labels = Nd4j.create(storys.size(), numberOfOutputsNeurons, maxLength);
		// Two labels: positive or negative
		// Because we are dealing with reviews of different lengths and only one
		// output at the final time step: use padding arrays
		// Mask arrays contain 1 if data is present at that time step for that
		// example, or 0 if data is just padding
		INDArray featuresMask = Nd4j.zeros(storys.size(), maxLength);
		INDArray labelsMask = Nd4j.zeros(storys.size(), maxLength);

		int[] temp = new int[2];
		for (int i = 0; i < storys.size(); i++) {
			List<String> tokens = allTokens.get(i);
			temp[0] = i;
			// Get word vectors for each word in review, and put them in the
			// training data
			for (int j = 0; j < tokens.size() && j < maxLength; j++) {
				String token = tokens.get(j);
				INDArray vector = wordVectors.getWordVectorMatrix(token);
				features.put(new INDArrayIndex[] { NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex.point(j) },
						vector);
				temp[1] = j;
				featuresMask.putScalar(temp, 1.0);
				// Word is present (not padding) for this example + time step ->
				// 1.0 in features mask
			}

			int lastIdx = Math.min(tokens.size(), maxLength);
			labels.putScalar(new int[] { i, pasitionOfTarget.get(i), lastIdx - 1 }, target.get(i));
			labelsMask.putScalar(new int[] { i, lastIdx - 1 }, 1.0);
			// Specify that an output exists at the final time step for this
			// example
		}
		return new DataSet(features, labels, featuresMask, labelsMask);
	}

	@Override
	public int totalExamples() {
		return inputFiles.length;
	}

	@Override
	public int inputColumns() {
		return vectorSize;
	}

	@Override
	public int totalOutcomes() {
		return 1;
	}

	@Override
	public void reset() {
		cursor = 0;
	}

	@Override
	public boolean resetSupported() {
		return true;
	}

	@Override
	public boolean asyncSupported() {
		return true;
	}

	@Override
	public int batch() {
		return batchSize;
	}

	@Override
	public int cursor() {
		return cursor;
	}

	@Override
	public int numExamples() {
		return totalExamples();
	}

	@Override
	public void setPreProcessor(DataSetPreProcessor preProcessor) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<String> getLabels() {
		return Arrays.asList("timeSpent");
	}

	@Override
	public boolean hasNext() {
		return cursor < numExamples();
	}

	@Override
	public DataSet next() {
		return next(batchSize);
	}

	@Override
	public void remove() {

	}

	@Override
	public DataSetPreProcessor getPreProcessor() {
		throw new UnsupportedOperationException("Not implemented");
	}

	/** Convenience method to get label for review */
	public boolean isPositiveReview(int index) {
		return index % 2 == 0;
	}

	public Set<String> getUnknownWords() {
		return unknownWords;
	}

}
