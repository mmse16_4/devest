package com.devest.deeplearning.service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.ui.api.UIServer;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;

import com.devest.deeplearning.lstm.support.LSTMNeuralNetworkSupport;
import com.devest.deeplearning.lstm.support.NeuralNetConfigurationDs;
import com.devest.deeplearning.lstm.support.NeuralNetworkMode;
import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;

public class DeeplearningService extends Object {
	private final static Logger LOG;
	static {
		LOG = LoggerSupport.setupLogger();
	}

	/**
	 * 
	 * @param path
	 *            do danych, path do modelu google, nazwa pliku wyjsciowego
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Preconditions.checkArgument(args.length == 5, "Zla liczba parametrow wejsciowych");
		LOG.info("Uruchomiono z argumentami: " + args[0] + " " + args[1] + " " + args[2]+" "+args[3]+" "+args[4]);
		Preconditions.checkNotNull(args[0]);
		Preconditions.checkNotNull(args[1]);
		Preconditions.checkNotNull(args[2]);
		Preconditions.checkNotNull(args[3]);
		Preconditions.checkNotNull(args[4]);
		NeuralNetConfigurationDs ds= new NeuralNetConfigurationDs();
		ds.setDataPath(args[0]);
		ds.setPathToGoogleModel(args[1]);
		ds.setPathToOutputDir(args[2]);
		ds.setNumberOfOutputsNeurons(Integer.parseInt(args[4]));
		if(StringUtils.equalsIgnoreCase(args[3], "R")){
			ds.setNeuralNetworkMode(NeuralNetworkMode.REGRESSION);
		}else{
			ds.setNeuralNetworkMode(NeuralNetworkMode.CLASSIFICATION);
		}
		DeeplearningService.performnLearning(ds);
	}

	public static void performnLearning(NeuralNetConfigurationDs ds) throws IOException {
		LSTMNeuralNetworkSupport support = new LSTMNeuralNetworkSupport();
		try {
			support.performLearning(ds);
		} catch (Exception e) {
			LOG.warning(e.getMessage());
			LOG.warning(Throwables.getStackTraceAsString(e));
			throw e;
		}
	}
}
