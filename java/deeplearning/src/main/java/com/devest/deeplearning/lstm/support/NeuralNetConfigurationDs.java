package com.devest.deeplearning.lstm.support;

public class NeuralNetConfigurationDs {
	private NeuralNetworkMode neuralNetworkMode;
	private String dataPath;
	private String pathToGoogleModel;
	private int batchSize=50;
	private int nEpochs=30;
	private int lstmOutputNeurons=256;
	private int numberOfOutputsNeurons;
	private String pathToOutputDir;
	
	public NeuralNetworkMode getNeuralNetworkMode() {
		return neuralNetworkMode;
	}
	public void setNeuralNetworkMode(NeuralNetworkMode neuralNetworkMode) {
		this.neuralNetworkMode = neuralNetworkMode;
	}
	public String getDataPath() {
		return dataPath;
	}
	public void setDataPath(String dataPath) {
		this.dataPath = dataPath;
	}
	public String getPathToGoogleModel() {
		return pathToGoogleModel;
	}
	public void setPathToGoogleModel(String pathToGoogleModel) {
		this.pathToGoogleModel = pathToGoogleModel;
	}
	public int getBatchSize() {
		return batchSize;
	}
	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}
	public int getnEpochs() {
		return nEpochs;
	}
	public void setnEpochs(int nEpochs) {
		this.nEpochs = nEpochs;
	}
	public int getLstmOutputNeurons() {
		return lstmOutputNeurons;
	}
	public void setLstmOutputNeurons(int lstmOutputNeurons) {
		this.lstmOutputNeurons = lstmOutputNeurons;
	}
	public int getNumberOfOutputsNeurons() {
		return numberOfOutputsNeurons;
	}
	public void setNumberOfOutputsNeurons(int numberOfOutputsNeurons) {
		this.numberOfOutputsNeurons = numberOfOutputsNeurons;
	}
	public String getPathToOutputDir() {
		return pathToOutputDir;
	}
	public void setPathToOutputDir(String pathToOutputDir) {
		this.pathToOutputDir = pathToOutputDir;
	}
}
