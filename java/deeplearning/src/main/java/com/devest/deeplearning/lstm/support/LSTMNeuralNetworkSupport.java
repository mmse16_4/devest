package com.devest.deeplearning.lstm.support;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.apache.commons.lang.IncompleteArgumentException;
import org.deeplearning4j.datasets.iterator.AsyncDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.eval.RegressionEvaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import com.devest.deeplearning.service.LoggerSupport;
import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;

public class LSTMNeuralNetworkSupport {
	private final static int VECTOR_SIZE = 300;// Size of the word vectors. 300
												// in the Google News model
	private final static int MAX_TOKENS_IN_REVIEW = 1000;
	private final static Logger LOG;
	static {
		LOG = LoggerSupport.setupLogger();
	}

	public void performLearning(NeuralNetConfigurationDs ds) throws IOException {
		// DataSetIterators for training and testing respectively
		// Using AsyncDataSetIterator to do data loading in a separate thread;
		// this may improve performance vs. waiting for data to load
		LOG.info("Wczytywanie Modelu Google");
		WordVectors wordVectors = WordVectorSerializer.loadGoogleModel(new File(ds.getPathToGoogleModel()), true,
				false);
		LOG.info("Tworzenie iteratorow");
		SentimentIterator trainIterator = new SentimentIterator(ds, wordVectors, MAX_TOKENS_IN_REVIEW, true);
		SentimentIterator testIterator =new SentimentIterator(ds, wordVectors, MAX_TOKENS_IN_REVIEW, false);
		DataSetIterator train = new AsyncDataSetIterator(trainIterator,1);
		DataSetIterator test = new AsyncDataSetIterator(testIterator, 1);

		// UIServer uiServer = UIServer.getInstance();
		// StatsStorage statsStorage = new InMemoryStatsStorage();
		// uiServer.attach(statsStorage);

		LOG.info("Konfiguracja sieci neuronowej");
		// Set up network configuration
		MultiLayerConfiguration conf = null;
		if (ds.getNeuralNetworkMode() == NeuralNetworkMode.REGRESSION) {
			conf = configureRegressionNeuralNetwork(ds);
		} else {
			conf = configureClassificationNeuralNetwork(ds);
		}

		MultiLayerNetwork net = new MultiLayerNetwork(conf);
		net.init();
		net.setListeners(new ScoreIterationListener(1));

		LOG.info("Starting training");
		for (int i = 0; i < ds.getnEpochs(); i++) {
			net.fit(train);
			train.reset();
			LOG.info("Epoch " + i + " complete. Starting evaluation:");

			// Run evaluation.
			if (ds.getNeuralNetworkMode() == NeuralNetworkMode.REGRESSION) {
				double[] result = performStatsForRegression(net, test);
				LOG.info(Arrays.toString(result));
				saveRegressionResultToFile(result, ds.getPathToOutputDir(), i);
			} else {
				List<String> result = performStatsForClassification(net, test,ds);
				saveClassificationResultToFile(result, ds.getPathToOutputDir(), i);
			}
			if(i==0){
				saveUnknownWordsToFile(trainIterator.getUnknownWords(), ds.getPathToOutputDir());
			}
		}
		LOG.info("----- Training complete -----");
	}

	private MultiLayerConfiguration configureRegressionNeuralNetwork(NeuralNetConfigurationDs ds) {
		Preconditions.checkArgument(ds.getNumberOfOutputsNeurons() == 1);
		return new NeuralNetConfiguration.Builder().optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
				.iterations(1).updater(Updater.RMSPROP).regularization(true).l2(1e-5).weightInit(WeightInit.XAVIER)
				.gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)
				.gradientNormalizationThreshold(1.0).learningRate(0.0018).list()
				.layer(0,
						new GravesLSTM.Builder().nIn(VECTOR_SIZE).nOut(ds.getLstmOutputNeurons()).activation("softsign")
								.build())
				.layer(1,
						new RnnOutputLayer.Builder().activation("identity").lossFunction(LossFunctions.LossFunction.MSE)
								.nIn(ds.getLstmOutputNeurons()).nOut(ds.getNumberOfOutputsNeurons()).build())
				.pretrain(false).backprop(true).build();
	}

	private MultiLayerConfiguration configureClassificationNeuralNetwork(NeuralNetConfigurationDs ds) {
		Preconditions.checkArgument(ds.getNumberOfOutputsNeurons() != 1);
		return new NeuralNetConfiguration.Builder()
	                .seed(123)    //Random number generator seed for improved repeatability. Optional.
	                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
	                .weightInit(WeightInit.XAVIER)
	                .updater(Updater.NESTEROVS).momentum(0.9)
	                .learningRate(0.005)
	                .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue)  //Not always required, but helps with this data set
	                .gradientNormalizationThreshold(0.5)
	                .list()
	                .layer(0, new GravesLSTM.Builder().activation("tanh").nIn(VECTOR_SIZE).nOut(ds.getLstmOutputNeurons()).build())
	                .layer(1, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
	                        .activation("softmax").nIn(ds.getLstmOutputNeurons()).nOut(ds.getNumberOfOutputsNeurons()).build())
	                .pretrain(false).backprop(true).build();
	}

	private List<String> performStatsForClassification(MultiLayerNetwork net, DataSetIterator test,NeuralNetConfigurationDs ds) {
		Evaluation evaluation = new Evaluation();
		List<String> result=new ArrayList<>();
		while (test.hasNext()) {
			DataSet t = test.next();
			INDArray features = t.getFeatureMatrix();
			INDArray lables = t.getLabels();
			INDArray inMask = t.getFeaturesMaskArray();
			INDArray outMask = t.getLabelsMaskArray();
			INDArray predicted = net.output(features, false, inMask, outMask);
			evaluation.evalTimeSeries(lables, predicted, outMask);
			List<String> resultForBatch = calculateResultRowForClassification(predicted, outMask, ds);
			result.addAll(resultForBatch);
		}
		test.reset();
		LOG.info(evaluation.stats());
		return result;
	}
	
	public static void saveUnknownWordsToFile(Set<String> unknownWords, String resultDir) {
		try {
			String pathToFile=resultDir+"/unknownWords.txt";
			LOG.info("Zapisywanie do pliku " + pathToFile);
			PrintWriter out = new PrintWriter(pathToFile);
			for (String r : unknownWords) {
				out.println(r);
			}
			out.close();
			LOG.info("Zapisano do pliku");
		} catch (FileNotFoundException e) {
			LOG.warning(Throwables.getStackTraceAsString(e));
		}
	}
	
	
	public static void saveClassificationResultToFile(List<String> result, String resultDir,int epoch) {
		try {
			String pathToFile=resultDir+"/epoch_"+epoch+".txt";
			LOG.info("Zapisywanie do pliku " + pathToFile);
			PrintWriter out = new PrintWriter(pathToFile);
			for (String r : result) {
				out.println(r);
			}
			out.close();
			LOG.info("Zapisano do pliku");
		} catch (FileNotFoundException e) {
			LOG.warning(Throwables.getStackTraceAsString(e));
		}
	}
	
	public static void saveRegressionResultToFile(double[] result, String resultDir,int epoch) {
		try {
			String pathToFile=resultDir+"/epoch_"+epoch+".txt";
			LOG.info("Zapisywanie do pliku " + pathToFile);
			PrintWriter out = new PrintWriter(pathToFile);
			for (double r : result) {
				out.println(r);
			}
			out.close();
			LOG.info("Zapisano do pliku");
		} catch (FileNotFoundException e) {
			LOG.warning(Throwables.getStackTraceAsString(e));
		}
	}


	private double[] performStatsForRegression(MultiLayerNetwork net, DataSetIterator test) {
		RegressionEvaluation evaluation = new RegressionEvaluation(1);
		double[] result = new double[test.totalExamples()];
		int lastPos = 0;
		while (test.hasNext()) {
			DataSet t = test.next();
			INDArray features = t.getFeatureMatrix();
			INDArray lables = t.getLabels();
			INDArray inMask = t.getFeaturesMaskArray();
			INDArray outMask = t.getLabelsMaskArray();
			INDArray predicted = net.output(features, false, inMask, outMask);
			evaluation.evalTimeSeries(lables, predicted, outMask);
			double[] currentResult = calculateResultRowForRegression(predicted, lables);
			System.arraycopy(currentResult, 0, result, lastPos, currentResult.length);
			lastPos += currentResult.length;
		}
		test.reset();
		LOG.info(evaluation.stats());
		return result;
	}

	/**
	 * Metoda oblicza macierz wyjściową, gdzie poszczególne wiersze to
	 * kolejne odserwacje, a kolumny to liczba tokenów (słów). W jednym
	 * wierszu tylko jedna wartość jest niezerowa i jest to pozycja, na
	 * której znajduje się ostatni wyraz w opisie.
	 * 
	 * @param predicted
	 * @param lables
	 */
	public static double[] calculateResultRowForRegression(INDArray predicted, INDArray lables) {
		Preconditions.checkNotNull(predicted);
		Preconditions.checkNotNull(lables);
		Preconditions.checkArgument(predicted.length() == lables.length());
		INDArray sub = predicted.sub(lables);
		Preconditions.checkArgument(sub.shape().length == 3);
		Preconditions.checkArgument(sub.shape()[1] == 1);
		int rows = sub.shape()[0];
		int columns = sub.shape()[2];
		double[] result = new double[rows];
		int resultIter = 0;
		boolean valueComputed = false;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				double val = sub.getDouble(i, 0, j);
				if (val != 0) {
					if (valueComputed) {
						throw new IncompleteArgumentException(
								"Po odjeciu w jednym z wierszy sa� rozne liczby wyrazow!");
					}
					result[resultIter] = val;
					resultIter++;
					valueComputed = true;
				}
			}
			valueComputed = false;
		}
		return result;
	}

	public static List<String> calculateResultRowForClassification(INDArray predicted, INDArray labelsMask,
			NeuralNetConfigurationDs ds) {
		Preconditions.checkNotNull(predicted);
		int rows = predicted.shape()[0]; // story
		int columns = predicted.shape()[2]; // moment wyjscia
		List<String> result = new ArrayList<>();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (labelsMask.getDouble(i, j) == 1) {
					double[] output=new double[ds.getNumberOfOutputsNeurons()];
					StringBuilder builder = new StringBuilder();
					for (int k = 0; k < ds.getNumberOfOutputsNeurons(); k++) { // iteracja po neuronach
						output[k]= predicted.getDouble(i, k, j);
					}
					int maxLocation = getMaxLocation(output);
					for (int k = 0; k < ds.getNumberOfOutputsNeurons(); k++) { // iteracja po neuronach
						if(k==maxLocation){
							builder.append("1");
						}else{
							builder.append("0");
						}
					}
					result.add(builder.toString());
				}
			}
		}
		return result;
	}
	
	private static int getMaxLocation(double[] array) {
	    int maxpos = 0;
	    double max = Double.MIN_VALUE;
	    for (int i = 0; i < array.length; i++) {
	        if (array[i] > max) {
	            max = array[i];
	            maxpos = i;
	        }
	    }
	    return maxpos;
	}
}
