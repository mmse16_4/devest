package com.devest.deeplearning.lstm.support;

public enum NeuralNetworkMode {
	REGRESSION,
	CLASSIFICATION;
}
