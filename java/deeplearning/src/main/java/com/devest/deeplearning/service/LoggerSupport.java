package com.devest.deeplearning.service;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerSupport {
	private static Logger logger = Logger.getLogger("DeeplearningService");  
    private static FileHandler fh; 
    public static Logger setupLogger(){
    	 try {
			fh = new FileHandler("./deepLearning-logger.log");
         logger.addHandler(fh);
         SimpleFormatter formatter = new SimpleFormatter();  
         fh.setFormatter(formatter);  
 		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
    	 return logger;
    }
}
