dutchFrom  <-  c(    'Á',    'Ä',    'A',    'Â',    'á',    'ä',    'a',    'â',    'É',    'Ë',    'E',    'E',    'é',    'ë',    'e',    'e',    'Í',    'I',    'I',    'Î',    'í',    'i',    'i',    'î',    'Ó',    'Ö',    'O',    'Ó',    'Ô',    'ó',    'ö',    'Ó',    'O',    'ô',    'Ú',    'Ü',    'U',    'U',    'ú',    'ü',    'u',    'u',    'Ý',    'Y',    'ý',    'y'  )
dutchTo    <-  c(    'A',    'A',    'A',    'A',    'a',    'a',    'a',    'a',    'E',    'E',    'E',    'E',    'e',    'e',    'e',    'e',    'I',    'I',    'I',    'I',    'i',    'i',    'i',    'i',    'O',    'O',    'O',    'O',    'O',    'o',    'o',    'o',    'o',    'o',    'U',    'U',    'U',    'U',    'u',    'u',    'u',    'u',    'Y',    'Y',    'y',    'y'  )
whitespace <- c('\t', '\n', '\r', '\v', '\f', '&nbsp;', '&lt;', '&gt;')

#' Metoda normalizujaca znaku z rodziny Dutch do ich wersji podstawowej
#'
#' @param x string zawierajacy zakazane znaki
#'
#' @return oczyszczony tekst bez znakow zakazanych
#' @export
#'
#' @examples
textNormalizer.normalizeDutchText <- function(x) {
  for (i in 1:length(dutchFrom))
    x <- gsub(dutchFrom[i], dutchTo[i], x)
  x
}

#textNormalizer.removeShortWords <- function(x) {
#  gsub("\\b[[:alpha:]]{1,2}\\b", " ", x)
#}

#' Metoda umozliwiajaca usuniecie znakow ktore nie sa alfanumeryczne
#'
#' @param x string zawierajacy zakazane znaki
#'
#' @return oczyszczony tekst bez znakow zakazanych
#' @export
#'
#' @examples
textNormalizer.removeNonAlpha <- function(x) {
  gsub("[^[:alpha:\\ ]*", "", x)
}
#' Metoda umozliwiajacaus uniecie znakow punktacji
#'
#' @param x string zawierajacy zakazane znaki
#'
#' @return oczyszczony tekst bez znakow zakazanych
#' @export
#'
#' @examples
textNormalizer.removePunctuation <- function(x) {
  gsub("[[:punct:]]", " ", x)
}

#' Metoda umozliwiajaca usuniecie nadmiarowych spacji
#'
#' @param x string zawierajacy zakazane znaki
#'
#' @return oczyszczony tekst bez znakow zakazanych
#' @export
#'
#' @examples
textNormalizer.removeWhiteSpace <- function(x) {
  for (i in 1:length(whitespace))
    x <- gsub(whitespace[i], " ", x)
  x
}

#' Metoda umozliwiajaca oczyszczenie tekstu ze znacznikow html
#'
#' @param x string zawierajacy zakazane znaki
#'
#' @return oczyszczony tekst bez znakow zakazanych
#' @export
#'
#' @examples
textNormalizer.removeHTMLTags <- function(x) {
  gsub("<[^>]*>", " ", x)
}

#' Metda umozliwiajaca oczyszczenie tekstu ze znacznikow w nawiazach wasatych
#'
#' @param x string zawierajacy zakazane znaki
#'
#' @return oczyszczony tekst bez znakow zakazanych
#' @export
#'
#' @examples
textNormalizer.removeMustacheTags <- function(x) {
  gsub("\\{[^\\{]*}", " ", x)
}

#' Title
#'
#' @param x string zawierajacy zakazane znaki
#'
#' @return oczyszczony tekst bez znakow zakazanych
#' @export
#'
#' @examples
textNormalizer.removeSquareTags <- function(x) {
  gsub("\\[[^\\{]*?\\]", " ", x)
}

#' Metoda oszyczszajaca zadany test
#' Odbywa się tu wywołanie mniejszych metod ktore sa opisane osobno.
#'
#' @param x string zawierajacy zakazane znaki
#'
#' @return oczyszczony tekst bez znakow zakazanych
#' @export
#'
#' @examples
textNormalizer.removeCodeTags <- function(x) {
  gsub("\\{code}[^\\{]*?\\{/code}", " ", x)
}

textNormalizer.performTextNormalization <- function(out) {
  out <- textNormalizer.removeHTMLTags(out)
  out <- textNormalizer.removeCodeTags(out)
  out <- textNormalizer.removeSquareTags(out)
  out <- textNormalizer.removeMustacheTags(out)
  out <- textNormalizer.normalizeDutchText(out)
  out <- textNormalizer.removeWhiteSpace(out)
  out <- tolower(out)
  out <- textNormalizer.removePunctuation(out)
  out <- textNormalizer.removeNonAlpha(out)
  out <- stripWhitespace(out)
  return(out)
}


