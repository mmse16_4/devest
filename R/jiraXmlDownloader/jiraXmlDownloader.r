downloadXMLFilesFromJira <- function(url=getCustomProperty('jiraURL'),
                                     destinationDirectory=getCustomProperty('jiraDataDirectoryPath')) {
  jiraULR <- url
  fileManager.checkAndRemoveAllXmlFiles(destinationDirectory)

  issuesSize <- 300
  i <- 0
  repeat {
    linkSource <- paste0(jiraULR,'/sr/jira.issueviews:searchrequest-xml/temp/SearchRequest.xml?jqlQuery=ORDER%20BY%20created%20ASC&tempMax=',issuesSize,'&pager%2Fstart=',issuesSize * i)
    linkTarget <- paste0(destinationDirectory,'/file_',i,'___',issuesSize,'_issues','.xml')
    download.file(url = linkSource, destfile = linkTarget)
    loginfo(sprintf('Saved to file %s', linkTarget))
    if (length(parser.retrieveAllItemsFromXml(linkTarget)) == 0) {
      file.remove(linkTarget)
      break
    }
    i <- i + 1
  }
}
